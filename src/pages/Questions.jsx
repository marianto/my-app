import React, { useState, useEffect } from 'react'
import { QuizContext } from '../context/ContextProvider'
import './Questions.css'
import { SandClock } from '../shared/components/SandClock'

export default function Questions() {


  const { data } = React.useContext(QuizContext)

  const useApiData = (apiKey) => {
    let apiDataArray = []
    data?.results.map((answer) => {
      if (!apiDataArray.includes(answer[apiKey])) {
        apiDataArray.push(answer[apiKey])
      }
    })
    return apiDataArray
  }

  const questionData = [
    {
      title: 'Who is the director of A New Hope?',
      answerOptions: useApiData('director'),
      correctAnswer: 'George Lucas'
    },

    {
      title: 'Who is the producer of the Phantom Menace?',
      answerOptions: useApiData('producer'),
      correctAnswer: 'Rick McCallum'
    },
    {
      title: 'What is the title of the film released on May 1999?',
      answerOptions: useApiData('title'),
      correctAnswer: 'The Phantom Menace'
    },
    {
      title: 'What Star Wars movie has Revenge of the Sith?',
      answerOptions: useApiData('episode_id'),
      correctAnswer: '3'
    },


  ]

  const [score, setScore] = useState(0);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [remainingTime, setRemainingTime] = useState(10)
  const [isDisabledQuestions, setIsDisabledQuestions] = useState(false)
  const [isFinished, setIsFinished] = useState(false)


  const isLastQuestion = currentQuestion === questionData.length - 1
  useEffect(() => {
    const interval = setInterval(() => {
      if (remainingTime > 0) setRemainingTime((prev) => prev - 1);
      if (remainingTime === 0) setIsDisabledQuestions(true);
    }, 1000)
    return () => clearInterval(interval)
  }, [remainingTime])


  function handleAnswerSubmit(currentQuestion, answer, e) {
    const currentCorrectAnswer = questionData[currentQuestion].correctAnswer
    if (currentCorrectAnswer === answer) setScore(score + 1);
    e.target.classList.add(currentCorrectAnswer === answer ? 'correct' : 'incorrect')
    startNewQuestionTimer(e)
  }

  function startNewQuestionTimer(e) {
    setTimeout(() => {
      if (isLastQuestion) {
        setIsFinished(true)
      } else {
        setCurrentQuestion(currentQuestion + 1)
      }
      setIsDisabledQuestions(false)
      setRemainingTime(10)
      e.target.classList.remove('correct', 'incorrect')
    }, 1000)
  }

  if (isFinished) return (
    <main className='quizContainer'>
      <div className='endGame'>
        <h3>¡Thank you for playing!</h3>
        <h3>You correct answers are {score} of {questionData.length}</h3>
        <button className='playAgainButton' onClick={() => (window.location.href = "/")}>Play again</button>
      </div>
    </main>

  )

  return (

    <div className='quizContainer'>
      <h3>How much do you know about Star Wars?</h3>
      <div className='titleSide'>
        <div className='questionNumber'><span>Question:</span>{currentQuestion + 1}<span>of</span>{questionData.length}</div>
        <div className='questionTitle'>{questionData[currentQuestion].title}</div>
        <div className='timeQuestions'> <SandClock /><h1>{remainingTime}</h1></div>
      </div>
      <div className='questionsSide'>

        {questionData[currentQuestion].answerOptions.map((answer, index) => (
          <button
            disabled={isDisabledQuestions}
            key={index}
            className='answerButton'
            onClick={(e) => handleAnswerSubmit(currentQuestion, answer, e)}
          >
            {answer}
          </button>
        ))}
      </div>
      <div className='nextQuestion'> {!remainingTime && !isFinished && <button className='nextQuestionButton' onClick={startNewQuestionTimer}>Next question</button>}</div>
    </div>


  );
}