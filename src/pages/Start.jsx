
import React from 'react'
import { Link } from "react-router-dom";
import { QuizContext } from '../context/ContextProvider'
import './Start.css'

export default function Start({ onQuizStart }) {

    const { data } = React.useContext(QuizContext)

    let apiFilms = []
    data?.results.map((film) => {
        apiFilms.push(film.title)
    })

    let randomFilm = apiFilms[Math.floor(Math.random() * apiFilms.length)];

    return (
        <div className='quizContainer'>
            <div className='startContent'>
                <div className='content'>
                    <h1>Hello! Welcome to Star Wars Quiz</h1>
                    <h3>Your film is: {randomFilm}</h3>
                    <Link to={{
                        pathname: "/questions",
                    }}>
                        <button className='button-start' onClick={onQuizStart}>Let's start</button>
                    </Link>
                </div>
            </div>
        </div>
    )
}
