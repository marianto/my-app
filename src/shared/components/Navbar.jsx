import React from 'react';
import './Navbar.css';
import { Link, NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { signOutAction } from '../../redux/userDucks'
import { withRouter } from 'react-router-dom';

const Navbar = (props) => {
    const dispatch = useDispatch()
    const logOutSesion = () => {
        dispatch(signOutAction())
        props.history.push('/login')
    }
    const active = useSelector(store => store.user.active)
    const userUpdatedProfile = useSelector(store => store.user.user)
    return (
        <div className="navbar">
            <Link className="navbar-brand" to="/">PlayPlanets</Link>
            <NavLink className="btn" to="/quiz" exact>Quiz </NavLink>
            <div>
                <div className="d-flex">
                    {
                        active ? (
                            <>
                                <h5 className="btn">Hello, {userUpdatedProfile.displayName}</h5>
                                <NavLink className="btn" to="/" exact>Dashboard</NavLink>
                                <NavLink className="btn" to="/settings" exact>Settings </NavLink>
                                <button className="btn" onClick={() => logOutSesion()}>
                                    Log out
                                </button>
                            </>
                        ) : (
                            <NavLink className="btn" to="/login" >Login</NavLink>
                        )

                    }
                </div>
            </div>
        </div>
    )
}

export default withRouter(Navbar);