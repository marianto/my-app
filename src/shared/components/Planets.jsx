import React from 'react'
import './Planets.css'
import { useDispatch, useSelector } from 'react-redux'
import {getPlanetsAction,nextPlanetsAction,previousPlanetsAction, planetDetailAction} from '../../redux/reduxDucks' 
import PlanetDetail from './PlanetDetail';
const Planets = () => {
    const dispatch = useDispatch();
    const planets = useSelector(store => store.planets.results);
    const next = useSelector(store => store.planets.next);
    const previous = useSelector(store => store.planets.previous);
    console.log(planets);

    React.useEffect(()=>{
        const fetchData = () => {
            dispatch(getPlanetsAction())
        }
        fetchData();
    }, [dispatch])

    return (
        <div className="row">
            <div className="col-md-6 mt-4">
                <h5> Discover your planets</h5>
                <ul>
                    {
                        planets.map(item => (
                            <li className="list-group-item" key = {item.name}>{item.name}
                                <button className="btn btn-dark btn-sm float-right" onClick={()=>dispatch(planetDetailAction(item.url))}></button>
                            </li>
                        ))
                    }
                </ul>
                <div className="buttons">
                    {next &&
                    <button className='next' onClick = {()=> dispatch(nextPlanetsAction())}>Next planets</button>
                    }
                    {previous &&
                    <button className='previous' onClick = {()=> dispatch(previousPlanetsAction())}>Previous planets</button>
                    }
                </div>
            </div>
            <div className="col-md-6 mt-4">
                <h5>Detail planet</h5>
                <PlanetDetail/>
            </div>
        </div>
    )
}

export default Planets
