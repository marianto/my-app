import React from 'react'
import { CgSandClock } from "react-icons/cg";
import './SandClock.css'
export function SandClock() {
  return (
    <div className='clock'>
      <CgSandClock className="timing" size={40} />
    </div>
  )
}
