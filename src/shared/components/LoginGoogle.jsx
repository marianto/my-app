import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { userLoginAction } from '../../redux/userDucks';
import { withRouter } from 'react-router-dom';

const Login = (props) => {
    const dispatch = useDispatch()
    const loading = useSelector(store => store.user.loading)
    const active = useSelector(store => store.user.active)

    React.useEffect(() => {
        if (active) {
            props.history.push('/')
        }
    }, [active, props.history])
    return (
        <div className="mt-5 text-center">
            <h3>Log In</h3>
            <hr />
            <button className="btn btn-dark" onClick={() => dispatch(userLoginAction())}
                disabled={loading}>Continue with Google</button>
        </div>
    )
}

export default Login