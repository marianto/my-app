import React from 'react';
import './Settings.css';
import { useSelector, useDispatch } from 'react-redux';
import {updateUserAction} from '../../redux/userDucks';

const Settings = () => {

  const userUpdatedProfile = useSelector(store => store.user.user)
  const loading = useSelector(store => store.user.loading)
  console.log(userUpdatedProfile);
  const [userName, setUserName] = React.useState(userUpdatedProfile.displayName);
  const [activeLogin, setActiveLogin] = React.useState(false);
  const dispatch = useDispatch()
  const updatedUser = () => {
    if(!userName.trim()){
      console.log('nombre vacio')
      return
    }
    dispatch(updateUserAction(userName));
    setActiveLogin(false)
  }
  return (
    <div className='mt-5 text-center'>
      <div className='card'>
        <div className='card-body'>
          <img className='imgProfile' src={userUpdatedProfile.photoURL} alt="" />
          <h5>Hello, {userUpdatedProfile.displayName}</h5>
          <p className='card-text'>Email: {userUpdatedProfile.email}</p>
          <button className="btn btn-dark" onClick={()=> setActiveLogin(true)}>Edit name</button>
          {
            loading &&
            <div className='card-body'>
              <div className='d-flex justify-content-center'>
                <div className='spinner-border' role='status'>
                  <span className='sr-only'></span>
                </div>
              </div>
            </div>
          }
          {
            activeLogin &&
          <div className='row justify-content-center'>
            <div className='col-md-5 '>
              <div className='input-group mb-3'>
                <input
                  type="text"
                  className="form-control"
                  value={userName}
                  onChange={e=>setUserName(e.target.value)}
                />
                <div className='input-group-append'>
                  <button className='btn btn-dark' type='button' onClick={()=>updatedUser()}>Update</button>
                </div>
              </div>
            </div>
          </div>
          }
  
        </div>
      </div>
    </div>
  )
}

export default Settings