import React from 'react'
import './PlanetDetail.css'
import { useDispatch, useSelector } from 'react-redux'
import { planetDetailAction } from '../../redux/reduxDucks';
const PlanetDetail = () => {
    const dispatch = useDispatch();
    React.useEffect(()=>{
        const fetchData = () => {
            dispatch(planetDetailAction())
        }
        fetchData();
    }, [dispatch])

    const planet = useSelector(store => store.planets.planetDetail);

  return planet ? (
    <div className='card'>
        <div className='card-body'>
            <img src="https://images.unsplash.com/photo-1462331940025-496dfbfc7564?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=911&q=80" alt='planet' className='img-fluid'/>
            <div className='card-title'>{planet.name}</div>
            <p className='card-text'>rotation period: {planet.rotation_period} </p>
            <p className='card-text'>orbital period: {planet.orbital_period}</p>
            <p className='card-text'>climate: {planet.climate}</p>
            <p className='card-text'>population: {planet.population}</p>
        </div>
    </div>
  ) : null
}

export default PlanetDetail