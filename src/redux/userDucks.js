//data inicial
import {auth, firebase, db} from '../firebase';
const dataInit = {
    loading: false,
    active: false
}
//types
const LOADING = 'LOADING'
const USER_ERR = 'USER_ERR'
const USER_EXIT = 'USER_EXIT'
const LOGOUT = 'LOGOUT'

//reducer
export default function userReducer (state = dataInit, action) {
    switch(action.type){
        case LOADING:
            return {...state, loading: true}
        case USER_ERR:
            return {...dataInit}
        case USER_EXIT:
            return {...state, loading: false, user: action.payload, active: true}
        case LOGOUT:
            return {...dataInit}
        default:
            return {...state}
    }
}
//action
export const userLoginAction = () => async(dispatch)=>{
    dispatch({
        type: LOADING
    })
    try {
        const provider = new firebase.auth.GoogleAuthProvider();
        const res = await auth.signInWithPopup(provider)
        //console.log(res.user);
        const userPlay = {
            uid: res.user.uid,
            email: res.user.email,
            displayName: res.user.displayName,
            photoURL: res.user.photoURL
        }
        const userDB = await db.collection('users').doc(userPlay.email).get();
        console.log(userDB);
        if(userDB.exists){
            dispatch({
                type: USER_EXIT,
                payload: userDB.data()
            })
            localStorage.setItem('user', JSON.stringify(userDB.data()))
        }else{
                //if user doesn't exit in firestone
                await db.collection('users').doc(userPlay.email).set(userPlay)
                dispatch({
                    type: USER_EXIT,
                    payload: userPlay
                })
                localStorage.setItem('user', JSON.stringify(userPlay))
        }   
    }catch(err){
        console.log(err)
        dispatch({
            type: USER_ERR
        })
    }
}

export const userActiveAction = () => (dispatch) => {
    if (localStorage.getItem('user')){
        dispatch({
            type: USER_EXIT,
            payload: JSON.parse(localStorage.getItem('user'))
        })
    }
}

export const signOutAction = () => (dispatch) => {
    auth.signOut()
    localStorage.removeItem('user')
    dispatch({
        type: LOGOUT
    })
}

export const updateUserAction = (updatedUser) => async(dispatch, getState) => {
    dispatch({
        type: LOADING
    })
    const {user} = getState().user
    console.log(user)
    try {
        await db.collection('users').doc(user.email).update({
            displayName: updatedUser
        })
        const userObject = {
            ...user,
            displayName: updatedUser
        }
        dispatch({
            type:USER_EXIT,
            payload: userObject
        })
        localStorage.setItem('user', JSON.stringify(userObject))
    }catch(err){
        console.log(err);
    }
 }