import axios from 'axios';
//constantes
const dataInit = {
   count: 0,
   next: null,
   previous: null,
   results: [],
   offset: 0
}
//types
const GET_PLANETS = 'GET_PLANETS';
const GET_NEXT_PLANETS = 'GET_NEXT_PLANETS';
const PLANET_DETAIL = 'PLANET_DETAIL';
//reducer
export default function pokeReducer(state = dataInit, action) {
    switch(action.type) {
        case GET_PLANETS:
            return {...state, ...action.payload}
        case GET_NEXT_PLANETS:
            return {...state, ...action.payload}
        case PLANET_DETAIL:
            return {...state, planetDetail: action.payload}
        default:
             return state

   }
}
//acciones
export const getPlanetsAction = () => async (dispatch, getState) => {
    if(localStorage.getItem('planetData')){
        //console.log('safe data')
        dispatch( {
            type: GET_PLANETS,
            payload: JSON.parse(localStorage.getItem('planetData'))
        }) 
        return;
    }
    try {
        //console.log('data from api')
        const res = await axios.get('https://swapi.dev/api/planets?offset=0/');
        console.log(res);
        dispatch( {
            type: GET_PLANETS,
            payload: res.data
        }) 
        localStorage.setItem('planetData', JSON.stringify(res.data))
    }catch(err) {
        console.log(err)
    }
}
export const nextPlanetsAction = () => async (dispatch, getState) => {
    const {next} = getState().planets;
    if(localStorage.getItem(next)){
        console.log('datos guardados');
        dispatch( {
            type: GET_PLANETS,
            payload: JSON.parse(localStorage.getItem(next))
        }) 
        return;
    }

    try {
        console.log('datos api');
        const res = await axios.get(next);
        dispatch( {
            type: GET_NEXT_PLANETS,
            payload: res.data
        }) 
        localStorage.setItem(next, JSON.stringify(res.data))

    }catch(err) {
        console.log(err)
    }
}
export const previousPlanetsAction = () => async (dispatch, getState) => {

    const {previous} = getState().planets
    if(localStorage.getItem(previous)){
        dispatch( {
            type: GET_PLANETS,
            payload: JSON.parse(localStorage.getItem(previous))
        }) 
        return;
    }
    try {
        const res = await axios.get(previous)
        dispatch({
            type: GET_NEXT_PLANETS,
            //utilizamos el mismo type porque la respuesta va a ser nuevamente lo que nos devuelva la api. va a ser una copia del state que va a reemplazar lo que haya cambiado
            payload: res.data
        })
        localStorage.setItem(previous, JSON.stringify(res.data))
    } catch (error) {
        console.log(error)
    }
}
export const planetDetailAction = (url= 'https://swapi.dev/api/planets/1/') => async(dispatch) => {
    if(localStorage.getItem(url)) {
        dispatch({
            type: PLANET_DETAIL,
            payload : JSON.parse(localStorage.getItem(url))
        })
        return
    }

    try {
        const res = await axios.get(url)
        //console.log(res.data)

        dispatch({
            type: PLANET_DETAIL,
            payload : {
                name : res.data.name,
                rotation_period: res.data.rotation_period,
                orbital_period: res.data.orbital_period,
                climate: res.data.climate,
                population: res.data.population
            }
        })
        localStorage.setItem(url, JSON.stringify({
            name : res.data.name,
            rotation_period: res.data.rotation_period,
            orbital_period: res.data.orbital_period,
            climate: res.data.climate,
            population: res.data.population
        })) 
    } catch (error) {
        console.log(error)
    }
}