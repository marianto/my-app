import {createStore, combineReducers, compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import reduxDucks from './reduxDucks';
import userReducer from './userDucks';
import {userActiveAction} from './userDucks';

const rootReducer = combineReducers({
    planets: reduxDucks,
    user: userReducer
})
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export default function generateStore() {
    const store = createStore( rootReducer, composeEnhancers( applyMiddleware(thunk) ))
    userActiveAction()(store.dispatch)
    return store
}