import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App.jsx';
import { Provider } from "react-redux";
import generateStore from "./redux/store";
import { QueryClient, QueryClientProvider } from 'react-query';
const store = generateStore();
const queryClient = new QueryClient()

ReactDOM.render(
  <QueryClientProvider client={queryClient}>
    <React.StrictMode>
      <Provider store={store}>
        <App />
      </Provider>
    </React.StrictMode>
  </QueryClientProvider>,
  document.getElementById('root')
);


