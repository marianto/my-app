import React, { useState } from 'react'
import { useQuery } from 'react-query'
import api from '../services/api'
import { Spinner } from '../shared/components/Spinner'

export const QuizContext = React.createContext()


const ContextProvider = (props) => {

    const { status, error, data } = useQuery("films", () =>
        api(`https://swapi.dev/api/films/`)
    );

    if (status === "loading") {
        return <Spinner />;
    }
    if (status === "error") {
        return <p>Error</p>;
    }
    if (status === "success") {


        return (
            <div>
                <QuizContext.Provider value={{ data }}>{props.children}</QuizContext.Provider>
            </div>
        )
    }

}

export default ContextProvider