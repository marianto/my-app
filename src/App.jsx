import React, { useState, useEffect } from "react";
import './App.css';
import Planets from "./shared/components/Planets";
import LoginGoogle from "./shared/components/LoginGoogle";
import Navbar from "./shared/components/Navbar";
import Settings from './shared/components/Settings';
import Questions from "./pages/Questions";
import { auth } from "./firebase";
import ContextProvider from "./context/ContextProvider";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Start from "./pages/Start";


function App() {


  const [firebaseUser, setFirebaseUser] = React.useState(false)

  React.useEffect(() => {
    const fetchUser = () => {
      auth.onAuthStateChanged(user => {
        console.log(user)
        if (user) {
          setFirebaseUser(user)
        } else {
          setFirebaseUser(null)
        }
      })
    }
    fetchUser();
  }, [])

  const RoutePrivate = ({ component, path, ...rest }) => {
    if (localStorage.getItem('user')) {
      const userStorage = JSON.parse(localStorage.getItem('user'));
      if (userStorage.uid === firebaseUser.uid) {
        return <Route component={component} path={path} {...rest} />
      } else {
        return <Redirect to="/login" {...rest} />
      }
    } else {
      return <Redirect to="/login" {...rest} />
    }
  }

  return firebaseUser !== false ? (
    <Router>
      <div className="container">
        <Navbar />
        <Switch>
          <RoutePrivate component={Planets} path="/" exact />
          <Route component={Settings} path="/settings" exact />
          <Route component={LoginGoogle} path="/login" exact />
          <ContextProvider>
            <Route component={Start} path="/quiz" exact />
            <Route component={Questions} path="/questions" exact />
          </ContextProvider>

        </Switch>
      </div>
    </Router>
  ) : (<div>Loading...</div>)

}

export default App;
